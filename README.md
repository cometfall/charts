# Cometfall Charts

This repository contains our two edited helm chart configurations for Valheim and Minecraft servers.

## Notes 

### General
- Both servers are set to provide their own external loadbalancer for access outside the cluster.
- You must provide domain routing information if you wish to use DNS instead of direct IP Links.
- Minecraft has the ability to deploy modded servers within its options.
- Valheim has support for Valheim+ mods.
- Valheim storage is changed from hostval storage to persistent volume claim
    (Enhanced data integrity in the event the host path is no longer avialable)

### Security
- Security information on our scanned containers is available here: https://gitlab.com/cometfall/containers

    (Not all containers are currently supported at this time for deployment)


## Deploying
Steps to deploying in any cluster.

1. Clone our project
2. Change directory into the desired server
3. Create a namespace for that server
4. Execute helm install with the server namespace
5. Wait for deployment
6. Edit variables for custom experience 
7. Enjoy!

# Valheim
```bash
cd valheim
kubectl create ns valheim
helm install --namespace=valheim valheim .
```

# Minecraft
```bash
cd minecraft
kubectl create ns minecraft
helm install --namespace=minecraft minecraft .
```

### References:

**Valheim:**

- Container:
    https://github.com/lloesche/valheim-server-docker



- Chart:
    https://github.com/Addyvan/valheim-k8s

**Minecraft:**


- Container:
    https://github.com/itzg/docker-minecraft-server


- Chart:
    https://github.com/itzg/minecraft-server-charts


